pico-8 cartridge // http://www.pico-8.com
version 16
__lua__
function _init()
	posx = 64
	posy = 64
	rectl = {}
end

function _update()
 if(btn(0)) then posx -= 1 end
 if(btn(1)) then posx += 1 end
 if(btn(2)) then posy -= 1 end
 if(btn(3)) then posy += 1 end
 
 if(btn(4)) then
 	r = crea_rect(posx,posy)
 	add(rectl,r)
 end
 
 if(btn(5)) then
 	c = crea_circ(posx,posy)
 	add(circl,c)
 end
 	
end

function _draw()
 cls()
 
 for r in all(rectl) do
		rect(r.x-2,r.y-2,r.x+2,r.y+2,4)
	end
	
	for c in all(circl) do
		circ(c.x,c.y,4,4)
	end
	
	rect(posx-2,posy-2,posx+2,posy+2,4)
end

-->8
function crea_rect(x,y)
	r = {}
	r.x = x
	r.y = y
	
	return r
end

function crea_circ(x,y)
	c = {}
	c.x = x
	c.y = y

	return c
end
