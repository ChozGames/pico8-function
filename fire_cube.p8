pico-8 cartridge // http://www.pico-8.com
version 16
__lua__
-- generer des cubes 
-- explosifs

function _init()
	lcube = {}
	fps = stat(7)
	t = 0
	cgen = 1
end

function _update()
	t += 1
	
	for c in all(lcube) do
		
		cube_upd(c)
		
		if isdead(c) then 
			del(lcube,c)
		end
	
		move(c)
	
	end
		
	add(lcube,gcube())

end

function _draw()
	cls()
	
	for c in all(lcube) do
		rect(c.x,c.y,c.x+5,c.y+5,c.col)
	 --	print(c.time,c.x,c.y)
	end
	
	--print(stat(95),0,0)
end



-->8
-- retourne le temps en 
-- seconde par rapport
-- aux fps
function sec(s)
	return flr(s/fps)
end

function msec(s)
	return s/fps
end

-- cree un nouveau cube
function gcube()
	c = {}
	c.x = 60
	c.y = 60
	c.col = 8 
	c.life = 2
	c.time = 0
	c.d = flr(rnd(2))
	
	return c
end

-- cube update
function cube_upd(c)
	c.time += 1
end


-- permet de savoir si on kill
-- un cube
function isdead(c)
	if sec(c.time) >= c.life then
		return true
	else
		return false
	end
end

function vmove(c)
	c.y -= 1
end

function hmove(c)
	c.x += rnd(2) - 1 
end

function move(c)
	vmove(c)
	hmove(c)
end

-- gestion du changement
-- de couleur
function ccol(c)
	if c.col == 8 or 
				c.col == 9 then
	 c.col += 1
	end
end

function cancol(c)
 	
end
