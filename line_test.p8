pico-8 cartridge // http://www.pico-8.com
version 16
__lua__
x = 30
y = 30
x2 = 60
y2 = 60
xinit = 30
yinit = 30
xmax = 60
ymax = 60

function _init()
end


function _update()
 x = movehp(x,2)
 x2 = movehp(x2,1)

 y = movevp(y,2)
 y2 = movevp(y2,1)
 
	x = resetp(x,xinit,xmax)
	x2 = resetp(x2,xinit+30,xmax)
	
	y = resetp(y,yinit,ymax)
	y2 = resetp(y2,yinit+30,ymax)
end


function _draw()
	cls()
	col = flr(rnd(16))
	line(xinit,yinit,x,30,col)
	line(xinit+30,yinit,60,y,col)
	line(xinit+30,yinit+30,x2,60,col)
	line(xinit,yinit+30,30,y2,col)
end
-->8
-- draw line function

spdline = 1

-- permet de faire 
-- avancer un point 
-- horizontalement
-- 1 : left
-- 2 : right
function movehp(x,d)
	if d == 1 then 
		x -= spdline
	elseif d == 2 then 
		x += spdline
	end
	
	return x	
end

-- permet de faire avance
-- un point verticalement
-- 1 : top
-- 2 : down
function movevp(y,d)
	if d == 1 then
	 y -= spdline
	elseif d == 2 then
		y += spdline
	end

	return y
end

-- permet de remettre un point
-- a sa valeur d'origine
function resetp(p,init,limit)
	-- p-init > limit
	if abs(p-init) > limit then
		return init
	end
	
	return p
end



