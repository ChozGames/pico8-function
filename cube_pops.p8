pico-8 cartridge // http://www.pico-8.com
version 16
__lua__
-- generer des cubes 
-- explosifs

function _init()
	lcube = {}
	fps = stat(7)
	t = 0
	cgen = 1
end

function _update()
	t += 1
	
	for c in all(lcube) do
		cube_upd(c)
		if isdead(c) then 
			del(lcube,c)
		end
	end
		
	add(lcube,gcube())

end

function _draw()
	cls()
	
	for c in all(lcube) do
		rect(c.x,c.y,c.x+5,c.y+5,c.col)
	 --	print(c.time,c.x,c.y)
	end
	
	--print(stat(95),0,0)
end



-->8
-- retourne le temps en 
-- seconde par rapport
-- aux fps
function sec(s)
	return flr(s/fps)
end

-- cree un nouveau cube
function gcube()
	c = {}
	c.x = flr(rnd(128))
	c.y = flr(rnd(128))
	c.col = flr(rnd(16))
	c.life = 2
	c.time = 0
	
	return c
end

-- cube update
function cube_upd(c)
	c.time += 1
end

function isdead(c)
	if sec(c.time) >= c.life then
		return true
	else
		return false
	end
end 
